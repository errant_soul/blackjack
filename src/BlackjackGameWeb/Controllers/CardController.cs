﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Blackjack.ViewModels.Card;
using BlackjackGame.BLL.Services;

namespace BlackjackGameWeb.Controllers
{
    [Route("api/[controller]")]
    public class CardController : Controller
    {
        private readonly CardService _cardService;
        private readonly IMapper _mapper;

        public CardController()
        {
            
        }

        public CardController(CardService cardService, IMapper mapper)
        {
            _cardService = cardService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var cards =  _cardService.GetAllAsync();
            var result = _mapper.Map<IEnumerable<CardViewModel>>(cards);
            
            return View(result);
        }
    }
}