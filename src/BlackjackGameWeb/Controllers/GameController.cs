﻿using System;
using System.Linq;
using System.Web.Mvc;
using Blackjack.ViewModels.Game;
using BlackjackGame.BLL.Logic;

namespace BlackjackGameWeb.Controllers
{
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        //private static LiveGame _game;
        private static GameLogic _game;

        public GameController()
        {
            if (_game == null)
            {
                _game = new GameLogic();
                _game.StartRound();
            }
        }

        // first check Db
        //public ViewResult Index()
        //{
        //    return View();
        //}

        #region Actions - NewGame, NewRound, AddCard, Stand

        [HttpGet]
        public ActionResult StartGame()
        {
            try
            {
                var model = _game.GetGameViewModel();

                return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public ActionResult StartNewRound()
        {
            _game = null;
            return RedirectToAction("StartGame");
        }

        public ActionResult AddCard()
        {
            var player = _game.Players.FirstOrDefault();
            player?.Hand.AddCard(_game.Deck, 1);

            var returnStatement = RedirectToAction("StartGame");

            return RedirectToAction("StartGame");
        }

        public ActionResult Stand()
        {
            var dealer = _game.Dealer;
            dealer?.Hand.AddCard(_game.Deck, 1);

            return RedirectToAction("StartGame");
        }

        #endregion

    }
}