﻿using System.Data.Entity;
using BlackjackGame.DAL.Repositories.Base;
using BlackjackGame.Entities.Entities;
using EnsureThat;

namespace BlackjackGame.DAL.Repositories
{
    public class GameRepository : BaseRepository<Game>
    {
        public GameRepository(DbSet<Game> dbSet) : base(dbSet)
        {
        }

        public Game GetGameByUserId(int userId)
        {
            Ensure.Any.IsNotDefault(userId);
            return DbSet.Find(userId);
        }
    }
}