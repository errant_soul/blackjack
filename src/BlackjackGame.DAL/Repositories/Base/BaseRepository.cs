﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BlackjackGame.DAL.Interfaces;
using BlackjackGame.Entities.Entities;
using BlackjackGame.Entities.Entities.Base;
using EnsureThat;

namespace BlackjackGame.DAL.Repositories.Base
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly DbSet<TEntity> DbSet;
        private readonly DbContext _context;

        protected BaseRepository(DbSet<TEntity> dbSet)
        {
            DbSet = dbSet;
        }

        //воть
        protected BaseRepository(DbContext context)
        {
            _context = context;
        }

        public TEntity Get(int id)
        {
            Ensure.Any.IsNotDefault(id);
            return DbSet.Find(id);
        }

        public async Task<TEntity> GetAsync(int id)
        {
            Ensure.Any.IsNotDefault(id);
            return await DbSet.FindAsync(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            Ensure.Any.IsNotNull(predicate);
            return DbSet.Where(predicate).ToList();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            Ensure.Any.IsNotNull(predicate);
            return await DbSet.Where(predicate).ToListAsync();
        }

        public void Create(TEntity entity)
        {
            Ensure.Any.IsNotNull(entity);
            DbSet.Add(entity);
        }

        public async Task CreateAsync(TEntity entity)
        {
            Ensure.Any.IsNotNull(entity);
            DbSet.Add(entity);

            await Task.CompletedTask;
        }

        public void Update(TEntity entity)
        {
            Ensure.Any.IsNotNull(entity);
            DbSet.Attach(entity);
        }

        public async Task UpdateAsync(TEntity entity)
        {
            Ensure.Any.IsNotNull(entity);
            DbSet.Attach(entity);

            await Task.CompletedTask;
        }

        public void Delete(int id)
        {
            var entity = DbSet.Find(id);

            Ensure.Any.IsNotNull(entity);
            DbSet.Remove(entity ?? throw new InvalidOperationException());
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await DbSet.FindAsync(id);

            Ensure.Any.IsNotNull(entity);
            DbSet.Remove(entity ?? throw new InvalidOperationException());

            await Task.CompletedTask;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}