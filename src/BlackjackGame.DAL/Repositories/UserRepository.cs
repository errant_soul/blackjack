﻿using System.Data.Entity;
using BlackjackGame.DAL.Repositories.Base;
using BlackjackGame.Entities.Entities;
using EnsureThat;

namespace BlackjackGame.DAL.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(DbSet<User> dbSet) : base(dbSet)
        {
        }

        public User GetUserByRoundId(int roundId)
        {
            Ensure.Any.IsNotDefault(roundId);
            return DbSet.Find(roundId);
        }

        public User GetUserByGameId(int gameId)
        {
            Ensure.Any.IsNotDefault(gameId);
            return DbSet.Find(gameId);
        }
    }
}