﻿using System.Data.Entity;
using BlackjackGame.DAL.Repositories.Base;
using BlackjackGame.Entities.Entities;
using EnsureThat;

namespace BlackjackGame.DAL.Repositories
{
    public class RoundRepository : BaseRepository<Round>
    {
        public RoundRepository(DbSet<Round> dbSet) : base(dbSet)
        {
        }

        public Round GetRoundByUserId(int userId)
        {
            Ensure.Any.IsNotDefault(userId);
            return DbSet.Find(userId);
        }

        public Round GetRoundByGameId(int gameId)
        {
            Ensure.Any.IsNotDefault(gameId);
            return DbSet.Find(gameId);
        }
    }
}