﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BlackjackGame.DAL.Repositories.Base;
using BlackjackGame.Entities.Entities;
using EnsureThat;

namespace BlackjackGame.DAL.Repositories
{
    public class CardRepository : BaseRepository<Card>
    {
        public CardRepository(DbSet<Card> dbSet) : base(dbSet)
        {
        }

        // для начальной проверки запроса к бд, потом можно убрать
        public Card GetCardByCardId(int cardId)
        {
            Ensure.Any.IsNotDefault(cardId);
            return DbSet.Find(cardId);
        }

        //по идеи, введу id пользователя и выдаст все карты пользователя
        public Card GetCardsByUserId(int userId)
        {
            Ensure.Any.IsNotDefault(userId);
            return DbSet.Find(userId);
        }

        //по идеи, введу id раунда и выдаст все карты раунда
        //
        //!!!
        public IEnumerable<Card> GetCardsByRoundId(int roundId)
        {
            Ensure.Any.IsNotDefault(roundId);
            return DbSet.Where(x => x.Round.Id == roundId);
        }

        // то же самое, но с id игры
        public Card GetCardByGameId(int gameId)
        {
            Ensure.Any.IsNotDefault(gameId);
            return DbSet.Find(gameId);
        }
    }
}
