﻿using System.Collections.Generic;
using BlackjackGame.DAL.EF;
using BlackjackGame.Entities.Entities;

namespace BlackjackGame.DAL.Initializer
{
    public class GameInitializer : System.Data.Entity.CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {

            var users = new List<User>
            {
                new User{Id = 1, FirstUserName = "Alex"},
            };

            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();

            var games = new List<Game>
            {
                new Game{Id = 1, Score = "0-0", Winner = "Player"},
                new Game{Id = 2, Score = "1-0", Winner = "Player"},
                new Game{Id = 3, Score = "1-1", Winner = "Dealer"},
            };

            games.ForEach(g => context.Games.Add(g));
            context.SaveChanges();

            var rounds = new List<Round>
            {
                new Round{Id = 1, GameId = 1, UserId = 1},

                // если будет два игрока, по идеи, запись будет такой
                //new Round{Id = 1, GameId = 1, UserId = 1},    -   лист карт первого игрока
                //new Round{Id = 1, GameId = 1, UserId = 2},    -   лист карт второго игрока

                new Round{Id = 2, GameId = 1, UserId = 1},
                new Round{Id = 3, GameId = 1, UserId = 1},
                new Round{Id = 4, GameId = 2, UserId = 1},
                new Round{Id = 5, GameId = 2, UserId = 1},
            };

            rounds.ForEach(r => context.Rounds.Add(r));
            context.SaveChanges();

            var cards = new List<Card>
            {
                new Card{CardRank = "Seven123 - after architecture upd", CardSuit = "Spade", Id = 1},
                new Card{CardRank = "Five", CardSuit = "Club", Id = 2},
                new Card{CardRank = "Ace", CardSuit = "Diamon", Id = 3},
                new Card{CardRank = "Quin", CardSuit = "Diamond", Id = 4},
                new Card{CardRank = "Ten", CardSuit = "Spade", Id = 5},
            };

            cards.ForEach(c => context.Cards.Add(c));
            context.SaveChanges();
        }
    }
}