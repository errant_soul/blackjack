﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack.ViewModels.Card
{
    public class CardViewModel
    {
        public string CardRank { get; set; }
        public string CardSuit { get; set; }


        /*
        public List<CardViewModel> PlayerCardRank { get; set; }
        public List<CardViewModel> DealerCardRank { get; set; }

        public List<CardViewModel> PlayerCardSuit { get; set; }
        public List<CardViewModel> DealerCardSuit { get; set; }
         */
    }
}
