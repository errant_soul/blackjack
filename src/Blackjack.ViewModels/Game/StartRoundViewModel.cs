﻿using System.Collections.Generic;
using Blackjack.ViewModels.Card;

namespace Blackjack.ViewModels.Game
{
    public class StartRoundViewModel
    {
        public List<CardViewModel> PlayerCards { get; set; }
        public List<CardViewModel> DealerCards { get; set; }
        public int DeckRemaining { get; set; }


        #region Checking the score

        public bool ModelIsBlackjackPlayer = false;
        public bool ModelIsBustedPlayer = false;
        public bool ModelIsLowerPlayer = false;
        public bool ModelIsOnTheMiddlePlayer = false;

        public bool ModelIsBlackjackDealer = false;
        public bool ModelIsBustedDealer = false;
        public bool ModelIsLowerDealer = false;
        public bool ModelIsOnTheMiddleDealer = false;

        #endregion


        public int PlayerScore { get; set; }
        public int DealerScore { get; set; }
    }
}