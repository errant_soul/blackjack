﻿using AutoMapper;
using Blackjack.ViewModels.Card;
using BlackjackGame.Entities.Entities;

namespace BlackjackGame.BLL.Configuration
{
    public abstract class AutoMapperConfiguration
    {
        protected readonly IMapper _mapper;

        protected AutoMapperConfiguration()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<Card, CardViewModel>()
                    .ForMember(dest => dest.CardRank,
                        opts => opts.MapFrom(src => src.CardRank))
                            .ForMember(dest=>dest.CardSuit,
                                opts=>opts.MapFrom(src=>src.CardSuit));
            });

            _mapper = config.CreateMapper();
        }

        //public static void ConfigureMappings()
        //{
        //    CreateMap<Card, CardViewModel>()
                //.ForMember(dest => dest.CardRank,
                //    opts => opts.MapFrom(src => new CardViewModel
                //    {
                //        CardRank = src.CardRank,
                //        CardSuit = src.CardSuit
        //            }));
        //}
    }
}

/*
    public MapperProfile()
        {
            CreateMap<ChangeOrderUpdateViewModel, ChangeOrder>(MemberList.None)
                .ForMember(item => item.Status, opt => opt.MapFrom(
                    o =>
                        o.RequiresApproval || o.NeedApprove ? ChangeOrderStatusTypes.Pending : ChangeOrderStatusTypes.Approved))
                .AfterMap((src, dest) =>
                {
                    dest.CurrencyList = new List<ChangeOrderCurrency>();
                    dest.CurrencyList.AddRange(GetCurrencyTypeList(src.Coins, CurrencyTypes.Coin));
                    dest.CurrencyList.AddRange(GetCurrencyTypeList(src.Notes, CurrencyTypes.Note));
                });
     */
