﻿using Blackjack.ViewModels.Game;
using BlackjackGame.BLL.Common_Logic;
using BlackjackGame.BLL.Models;
using System.Collections.Generic;
using System.Linq;
using Blackjack.ViewModels.Card;
using BlackjackGame.BLL.Configuration;

namespace BlackjackGame.BLL.Logic
{
    public class GameLogic : AutoMapperConfiguration
    {
        public List<GamePlayer> Players { get; set; }
        public GameDealer Dealer { get; set; }
        public CardDeck Deck { get; set; }

        private int amountOfCardsToStartRound = 2;
        public int dealersScore, playersScore;

        public GameLogic()
        {
            Players = new List<GamePlayer>();
            Dealer = new GameDealer(Players);
        }

        public void StartRound()
        {
            if (Deck == null)
            {
                Deck = new CardDeck(true);
            }

            var firstPlayer = new GamePlayer(new Hand(new List<Card>()));
            firstPlayer.Hand.AddCard(Deck, amountOfCardsToStartRound);
            Players.Add(firstPlayer);

            var dealer = new GameDealer(new List<GamePlayer>());
            dealer.Hand.AddCard(Deck, amountOfCardsToStartRound);
            Dealer = dealer;
        }

        public StartRoundViewModel GetGameViewModel()
        {
            var result = new StartRoundViewModel();

            var foo = _mapper.Map<List<CardViewModel>>(Players.FirstOrDefault()?.Hand.Cards);
            var foo1 = _mapper.Map<List<CardViewModel>>(Dealer.Hand.Cards);

            result.PlayerCards = foo;
            result.DealerCards = foo1;
            result.DeckRemaining = Deck.CardDeckCounter();
            result.DealerScore = (int)Dealer.Hand.CalculateScoreHighLow();
            result.PlayerScore = (int)Players.FirstOrDefault()?.Hand.CalculateScoreHighLow();


            #region Checking hands

            // check player`s hand
            if (Players.FirstOrDefault()?.Hand.IsBlackjack == true)
                result.ModelIsBlackjackPlayer = true;

            if (Players.FirstOrDefault()?.Hand.IsBusted == true)
                result.ModelIsBustedPlayer = true;

            if (Players.FirstOrDefault()?.Hand.Score < 21)
                result.ModelIsLowerPlayer = true;

            if (Players.FirstOrDefault()?.Hand.IsBlackjack != true && Players.FirstOrDefault()?.Hand.Score == 21)
                result.ModelIsOnTheMiddlePlayer = true;

            // check dealer`s hand
            if (Dealer.Hand.IsBlackjack == true)
                result.ModelIsBlackjackDealer = true;

            if (Dealer.Hand.IsBusted == true)
                result.ModelIsBustedDealer = true;

            if (Dealer.Hand.Score < 21)
                result.ModelIsLowerDealer = true;

            if (Dealer.Hand.IsBlackjack != true && Dealer.Hand.Score == 21)
                result.ModelIsOnTheMiddleDealer = true;

            #endregion


            return result;
        }

    }
}
