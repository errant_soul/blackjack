﻿using System;

namespace BlackjackGame.BLL.Common_Logic
{
    public class CardShoe : CardDeck
    {
        public int NumberOfDecks { get; private set; }

        public CardShoe(int numberOfDecks)
            : base()
        {
            if (numberOfDecks <= 0)
                throw new InvalidOperationException("Shoe must have at least one deck.");

            NumberOfDecks = numberOfDecks;

            for (int i = 0; i < numberOfDecks; i++)
            {
                var deck = new CardDeck(true);
                _cards.AddRange(deck.Cards);
            }
        }
    }
}