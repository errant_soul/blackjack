﻿using System.Collections.Generic;
using System.Linq;
using Blackjack.ViewModels.Card;
using BlackjackGame.BLL.Common_Logic;

namespace BlackjackGame.BLL.Logic
{
    public class Hand
    {
        //private readonly IEnumerable<Card> _cards;
        public List<Card> Cards { get; private set; }
        public bool IsBlackjack { get; private set; }
        public bool IsBusted => Score > 21;
        public int Score => CalculateScoreHighLow();


        public Hand(IEnumerable<Card> cards)
        {
            Cards = cards.ToList();
        }

        public int CalculateScoreHighLow()
        {
            var aceCount = 0;
            var score = 0;

            foreach (var card in Cards)
            {
                if (card.CardRank == CardRank.Ace)
                {
                    aceCount++;
                }
                else if (card.CardRank >= CardRank.Ten)
                {
                    score += 10;
                }
                else
                {
                    score += (int)card.CardRank;
                }
            }

            var lowScore = score + aceCount;
            var highScore = 0;
            if (aceCount >= 1)
            {
                var tmp = aceCount;
                highScore = lowScore + tmp * 10;
            }

            else
                highScore = lowScore;

            if (highScore > 21)
                return lowScore;

            return highScore;
        }

        public void AddCard(CardDeck deck, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Cards.Add(deck.DealCard());
            }
            IsBlackjack = Cards.Count() == 2 && Score == 21;
        }
    }
}