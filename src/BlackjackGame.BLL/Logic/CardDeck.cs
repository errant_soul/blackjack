﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackjackGame.BLL.Common_Logic
{
    public class CardDeck
    {
        public List<Card> _cards = new List<Card>();

        public IEnumerable<Card> Cards
        {
            get { return _cards; }
        }

        public int CardDeckCounter()
        {
            return _cards.Count;
        }

        protected CardDeck()
        {
            if (_cards == null)
            {
                _cards = new List<Card>();
            }
        }

        public CardDeck(bool shuffled = false)
        {
            var ranks = Enum.GetValues(typeof(CardRank)).Cast<CardRank>().ToList();
            var suits = Enum.GetValues(typeof(CardSuit)).Cast<CardSuit>().ToList();

            suits.ForEach(suit =>
                ranks.ForEach(rank =>
                    _cards.Add(new Card(suit, rank))));

            if (shuffled)
                Shuffle();
        }

        public virtual void Shuffle()
        {
            _cards = _cards.OrderBy(x => Guid.NewGuid()).ToList();
        }

        public virtual Card DealCard()
        {
            if (!_cards.Any())
                throw new InvalidOperationException("Deck is out of cards.");

            Card card = _cards.ElementAt(0);
            _cards.RemoveAt(0);
            return card;
        }
    }
}