﻿namespace BlackjackGame.BLL.Common_Logic
{
    public class Card
    {
        public CardSuit CardSuit { get; private set; }
        public CardRank CardRank { get; private set; }

        public Card(CardSuit suit, CardRank rank)
        {
            CardSuit = suit;
            CardRank = rank;
        }

    }

    public enum CardRank
    {
        Ace = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13
    }

    public enum CardSuit
    {
        Club,
        Diamond,
        Heart,
        Spade
    }
}