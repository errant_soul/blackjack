﻿using System.Collections.Generic;
using BlackjackGame.BLL.Common_Logic;
using BlackjackGame.BLL.Logic;

namespace BlackjackGame.BLL.Models
{
    public class GameDealer
    {
        public Hand Hand { get; private set; }
        //public GamePlayer Player { get; private set; }
        //private CardShoe _shoe;
        public List<GamePlayer> players;

        public GameDealer(List<GamePlayer> players, Hand listOf)
        {
            Hand = new Hand(new List<Card>());
        }

        public GameDealer(List<GamePlayer> players)
        {
            this.players = players;
            Hand = new Hand(new List<Card>());
        }

        // mb will use in future
        //public void RefreshShoe()
        //{
        //    _shoe = new CardShoe(4);
        //}
    }
}