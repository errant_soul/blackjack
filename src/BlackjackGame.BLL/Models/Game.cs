﻿using System.Collections.Generic;
using BlackjackGame.BLL.Models;

namespace BlackjackGame.BLL
{
    public class Game
    {
        private GameDealer _dealer;
        private List<GamePlayer> _players;

        public Game()
        {
        }

        public Game(GameDealer dealer, List<GamePlayer> players)
        {
            _dealer = dealer;
            _players = players;
        }
    }
}