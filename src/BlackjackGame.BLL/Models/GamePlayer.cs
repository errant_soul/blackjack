﻿using System;
using System.Collections.Generic;
using BlackjackGame.BLL.Common_Logic;
using BlackjackGame.BLL.Logic;

namespace BlackjackGame.BLL.Models
{
    public class GamePlayer
    {
        public string Id { get; private set; }
        public Hand Hand { get; set; }

        public GamePlayer(Hand listOfHand)
        {
            Id = Guid.NewGuid().ToString();
            Hand = new Hand(new List<Card>());
        }
    }
}