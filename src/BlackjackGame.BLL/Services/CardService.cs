﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Blackjack.ViewModels.Card;
using BlackjackGame.BLL.Interfaces;
using BlackjackGame.BLL.Services.Base;
using BlackjackGame.DAL.Interfaces;
using BlackjackGame.DAL.Repositories;
using BlackjackGame.Entities.Entities;

namespace BlackjackGame.BLL.Services
{
    public class CardService : Service<Card>
    {

        public CardService(IRepository<Card> repository) : base(repository)
        {
        }

        public async Task<IEnumerable<CardViewModel>> GetCardById(int cardId)
        {
            var entities = await Repository.GetAsync(cardId);

            return Mapper.Map<IEnumerable<CardViewModel>>(entities);
        }
    }
}
