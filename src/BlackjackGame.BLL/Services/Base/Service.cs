﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BlackjackGame.BLL.Interfaces;
using BlackjackGame.DAL.Interfaces;
using BlackjackGame.Entities.Entities.Base;

namespace BlackjackGame.BLL.Services.Base
{
    public abstract class Service<TEntity> : IService<TEntity> where TEntity : Entity
    {
        protected readonly IRepository<TEntity> Repository;

        protected Service(IRepository<TEntity> repository)
        {
            Repository = repository;
        }

        public TEntity Get(int id)
        {
            return Repository.Get(id);
        }

        public async Task<TEntity> GetAsync(int id)
        {
            return await Repository.GetAsync(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await Repository.GetAllAsync();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.Find(predicate);
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Repository.FindAsync(predicate);
        }

        public void Create(TEntity entity)
        {
            Repository.Create(entity);
            Repository.SaveChanges();
        }

        public async Task CreateAsync(TEntity entity)
        {
            await Repository.CreateAsync(entity);
            await Repository.SaveChangesAsync();
        }

        public void Update(TEntity entity)
        {
            Repository.Update(entity);
            Repository.SaveChanges();
        }

        public async Task UpdateAsync(TEntity entity)
        {
            await Repository.UpdateAsync(entity);
            await Repository.SaveChangesAsync();
        }

        public void Delete(int id)
        {
            Repository.Delete(id);
            Repository.SaveChanges();
        }

        public async Task DeleteAsync(int id)
        {
            await Repository.DeleteAsync(id);
            await Repository.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            Repository.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await Repository.SaveChangesAsync();
        }
    }
}