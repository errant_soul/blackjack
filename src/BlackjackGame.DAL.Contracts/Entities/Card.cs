﻿using BlackjackGame.Entities.Entities.Base;

namespace BlackjackGame.Entities.Entities
{
    public class Card : Entity
    {
        public string CardRank { get; set; }

        public string CardSuit { get; set; }
        public int Lox { get; set; }

        public Round Round { get; set; }
    }
}