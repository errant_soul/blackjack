﻿using System.Collections.Generic;
using BlackjackGame.Entities.Entities.Base;

namespace BlackjackGame.Entities.Entities
{
    public class Round : Entity
    {
        public int GameId { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public Game Game { get; set; }

        public ICollection<Card> Cards { get; set; }
    }
}