﻿using System.Collections.Generic;
using BlackjackGame.Entities.Entities.Base;

namespace BlackjackGame.Entities.Entities
{
    public class Game : Entity
    {
        public string Winner { get; set; }

        //public int RoundId { get; set; }

        // means not TOTAL score(player - 4 wins), but [9(player)-4(dealer)]
        public string Score { get; set; }

        public ICollection<User> Users { get; set; }

        public ICollection<Round> Rounds { get; set; }
    }
}