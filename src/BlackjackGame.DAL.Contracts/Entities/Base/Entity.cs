﻿namespace BlackjackGame.Entities.Entities.Base
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}