﻿using System.Collections.Generic;
using BlackjackGame.Entities.Entities.Base;

namespace BlackjackGame.Entities.Entities
{
    public class User : Entity
    {
        public string FirstUserName { get; set; }

        public ICollection<Game> Games { get; set; }
    }
}